# Installs the basic dependencies
echo "Update Ubuntu"
sudo apt-get -y update
sudo apt-get -y upgrade

echo "Installing basic packages"
sudo apt-get -y install \
  curl \
  git \
  vim

# Default editor
sudo update-alternatives --set editor /usr/bin/vim.basic

# git setup
echo "Setting up git config"
git config --global user.name "Rasmus Toft Lauridsen"
read -p "git global user.email: " gitEmail
git config --global user.email $gitEmail

echo "Setting up git credential storage"
sudo apt-get -y install \
  libsecret-1-0 \
  libsecret-1-dev
cd /usr/share/doc/git/contrib/credential/libsecret
sudo make
cd ~/
git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret

# install fzf
echo "Installing fzf"
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# Setup development folder
echo "Setting up development folder"
mkdir -p ~/development

# Git terminal?
# lpass?
echo "Installing lastpass cli"
sudo apt-get --no-install-recommends -yqq install \
  bash-completion \
  build-essential \
  cmake \
  libcurl4  \
  libcurl4-openssl-dev  \
  libssl-dev  \
  libxml2 \
  libxml2-dev  \
  libssl1.1 \
  pkg-config \
  ca-certificates \
  xclip \
  asciidoc \
  xsltproc
git clone https://github.com/lastpass/lastpass-cli.git ~/.local/lastpass-cli
cd ~/.local/lastpass-cli
make
sudo make install
sudo make install-doc
cd ~/

echo "Logging into lastpass"
lpass login --trust rasmus@lauridsen.me
# local keys?

echo "Cloning bootsrap scripts repository"
git clone --config  "https://estei:$(lpass show --password gitlab.com)@gitlab.com/estei/machine-bootstrap.git" ~/.local/machine-bootstrap

# machine-bootstrap

Scripts for bootstrapping a new machine

To get started run the following script

```sh
wget -q -O - https://gitlab.com/estei/machine-bootstrap/raw/master/init.sh | sudo bash

```

## helm completion

```sh
source <(helm completion bash)
```
